<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use App\pelapor;
use Illuminate\Foundation\Auth\AuthenticatesUsers;


class pelaporController extends Controller
{
    use AuthenticatesUsers;
        
    public function index()
    {
        $tampil = pelapor::all();
        
        return view('index',compact('tampil'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)

    {

    	
   		
   		// $path = $request->file->store('file');
   		// $fileName   = $file->getClientOriginalName();
        // $request->file('file')->move("upload/", $fileName);
    	
    	
        /*$ext = $file->getClientOriginalExtension();
        $newName = rand(100000,1001238912).".".$ext;
        $file->move('uploads/file',$newName);
        $data->file = $newName;    	*/

        if($request->hasFile('file')){
        	
        	$filename = $request->file->getClientOriginalName();
        	$request->file->storeAs('public/upload',$filename);

        }	

        $pelapor = new pelapor;

        $filename = $request->file->getClientOriginalName();
       	$pelapor -> file = $filename;
       	
       	$pelapor -> status = $request -> status;
        $pelapor -> namabencana = $request -> namabencana;
        $pelapor -> lokasi = $request -> lokasi;
        $pelapor -> lokasikabupaten = $request -> lokasikabupaten;
        $pelapor -> kecamatan = $request -> kecamatan;
        $pelapor -> nama = $request -> nama;
        $pelapor -> email = $request -> email ;
        $pelapor -> nohp = $request -> nohp ;
        $pelapor -> korbanmati = $request -> korbanmati ;
        $pelapor -> korbanberat = $request -> korbanberat ;
        $pelapor -> korbansedang = $request -> korbansedang ;
        $pelapor -> korbanringan = $request -> korbanringan ;
        $pelapor -> save();
        return redirect()->back();
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = pelapor::find($id);
        $pelapor -> lokasi = $request -> lokasi;
        $pelapor -> nama = $request -> nama;
        $pelapor -> email = $request -> email ;
        $pelapor -> nohp = $request -> nohp ;
        $update -> save();
 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
