@extends('masterBNPB')
@section('content')

<style>
  hr{
    border-color: #fed136;
    border-width: 3px;
    max-width: 50px;
  }
</style>
<!-- Modal -->

<style>
  .modal-content{
    background-color: white;
    border: none;
  }

.modal-footer{
  border-top: none;
  background-color: #fed136;
}
.modal-body{
  padding: 0rem;
}
</style>
      <!-- Header -->
      <header class="masthead">
        <div class="container">
          <div class="intro-text">
            <div class="row">
              <div class="col-lg-12 text-center">
                <h2 class="section-heading text-uppercase">LIST NEWS</h2><br>
                <hr><br>
                <h3 class="section-subheading text-muted"></h3>

              </div>
            </div>
            <table background-color="black" class="table">
                      <thead class="table-info">
                          <tr>
                              <th>Bencana</th>
                              <th>Meninggal</th>
                              <th>Berat</th>
                              <th>Sedang</th>
                              <th>Ringan</th>
                              <th>Lokasi Bencana</th>
                              <th></th>
                          </tr>
                      </thead>
                      <tbody>
                          <tr>
                              <td>Gunung Api Meletus</td>
                              <td>10</td>
                              <td>5</td>
                              <td>3</td>
                              <td>2</td>
                              <td>Kaliurang, Pakem, Sleman, Yogyakarta</td>
                              <td>
                                <button type="button" class="btn btn-primary btn-m " data-toggle="modal" data-target="#ModalPost1" >Edit</button>
                              </td>
                          </tr>
                          <tr>
                            <td>Tornado</td>
                            <td>25</td>
                            <td>30</td>
                            <td>27</td>
                            <td>13</td>
                            <td>Sardonoharjo, Ngaglik,Sleman, Yogyakarta</td>
                              <td>
                                <button type="button" class="btn btn-primary btn-m " data-toggle="modal" >Edit</button>
                              </td>
                          </tr>
                          <tr>
                            <td>Tsunami</td>
                            <td>500</td>
                            <td>680</td>
                            <td>80</td>
                            <td>1203</td>
                            <td>Parangkusumo, Kretek, Bantul, Yogyakarta</td>
                              <td>
                                <button type="button" class="btn btn-primary btn-m " data-toggle="modal" >Edit</button>
                              </td>
                          </tr>
                      </tbody>
                  </table>
          </div>
        </div>

        <style>
        html {
          font-family: Lato, 'Helvetica Neue', Arial, Helvetica, sans-serif;
          font-size: 14px;
        }

        .table {
          border: none;
          background-color: #212529;
        }

        .table-definition thead th:first-child {
          pointer-events: none;
          background: white;
          border: none;
        }

        .table-info{
          background-color: #212529;
          border: none;
        }

        .table td {
          vertical-align: middle;
          border-top: black;
        }

        .page-item > * {
          border: none;
        }

        .custom-checkbox {
        min-height: 1rem;
        padding-left: 0;
        margin-right: 0;
        cursor: pointer;
        }
        .custom-checkbox .custom-control-indicator {
          content: "";
          display: inline-block;
          position: relative;
          width: 30px;
          height: 10px;
          background-color: #818181;
          border-radius: 15px;
          margin-right: 10px;
          -webkit-transition: background .3s ease;
          transition: background .3s ease;
          vertical-align: middle;
          margin: 0 16px;
          box-shadow: none;
        }
          .custom-checkbox .custom-control-indicator:after {
            content: "";
            position: absolute;
            display: inline-block;
            width: 18px;
            height: 18px;
            background-color: #f1f1f1;
            border-radius: 21px;
            box-shadow: 0 1px 3px 1px rgba(0, 0, 0, 0.4);
            left: -2px;
            top: -4px;
            -webkit-transition: left .3s ease, background .3s ease, box-shadow .1s ease;
            transition: left .3s ease, background .3s ease, box-shadow .1s ease;
          }
        .custom-checkbox .custom-control-input:checked ~ .custom-control-indicator {
          background-color: #84c7c1;
          background-image: none;
          box-shadow: none !important;
        }
          .custom-checkbox .custom-control-input:checked ~ .custom-control-indicator:after {
            background-color: #84c7c1;
            left: 15px;
          }
        .custom-checkbox .custom-control-input:focus ~ .custom-control-indicator {
          box-shadow: none !important;
        }
        </style>


          </div>
        </div>
      </header>

      <!--MODAL LAPORAN-->
          <div class="modal fade" id="ModalPost1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">

                <div class="modal-body">

                  <div class="wrap-login100">
                    <h2 class="section-heading text-uppercase">EDIT POST</h2><br>
                    <br><br>
                    <div class="row">
                      <div class="col">
                        Nama Bencana
                      </div>
                      <div class="col">
                        : Gunung Merapi Meletus
                      </div>
                    </div>
                    <div class="row">
                      <div class="col">

                      </div>
                      <div class="col">
                        <input class="form-control" type="text" name="" value="" placeholder="Edit Nama Bencana">
                      </div>
                    </div>
                    <div class="row">
                      <div class="col">
                        Korban Berat
                      </div>
                      <div class="col">
                        : 5
                      </div>
                    </div>
                    <div class="row">
                      <div class="col">
                        Korban Sedang
                      </div>
                      <div class="col">
                        : 3
                      </div>
                    </div>
                    <div class="row">
                      <div class="col">
                        Korban Ringan
                      </div>
                      <div class="col">
                        : 2
                      </div>
                    </div>
                    <div class="row">
                      <div class="col">
                        Jumlah Korban
                      </div>
                      <div class="col">
                        : 20
                      </div>
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>

@endsection
